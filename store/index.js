const state = ()=> ({
    token: "",
    siteName: "Nuxt.js Quick Starter",
    currentPage: {
        title: ""
    },
});
const mutations = {
    CURRENT_PAGE(state, pageTitle){
        state.currentPage.title = pageTitle;
    }
}
const actions = {

}
const getters = {

}

export default {
    state,
    mutations,
    actions,
    getters
}